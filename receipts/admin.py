from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt

# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    category_display = [
        'name',
        'owner'
    ]

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    account_display = [
        'name',
        'number',
        'owner'
    ]

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    category_display = [
        'vendor',
        'total',
        'tax',
        'date',
        'purchaser',
        'category',
        'account'
    ]
